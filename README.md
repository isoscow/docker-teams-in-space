# Docker Teams In Space Project

## Overview
This is an Atlassian Docker build that loads all the dependencies to run Teams in Space.
Running on an Ubuntu 14.04 image.  

# Prerequisites 
Make sure you have Docker installed on your computer. If not, visit [Docker-download](https://www.docker.com/community-edition#download) and download the community version for your operating system. Make sure you have at least 4 CPUs and 8.0 GB of Memory running under your docker preferences. Also, make sure you are logged into your docker account `docker login` followed by your `username` and `password`.

Copy your private SSH key into the home directory of the this repository `~/docker-teams-in-space/id_rsa`. Rename or make sure your copied SSH key is named `id_rsa`. 

# Build instructions if you are pulling from Bitbucket repository (Primary)

	git clone git@bitbucket.org:davidglennjenkins/docker-teams-in-space.git
	cd docker-teams-in-space/docker-java8
	docker build -t docker-java8 .
	cd ..
	docker build -t ubuntu-tis-full . 
	docker run -p 2430:2430 -p 7990-8200:7990-8200 -it ubuntu-tis-full /bin/bash

## Start only the applications you need (Primary)
By adding the `--entrypoint` flag, you are able to startup only the applications you need. 

### Jira + Confluence
    docker run -p 2430:2430 -p 8080:8080 -p 8090:8090 --entrypoint /root/boot/jira+confluence.sh -it ubuntu-tis-full /bin/bash   

### Jira + Confluence + Bitbucket
    docker run -p 2430:2430 -p 7990:7990 -p 8080:8080 -p 8090:8090 --entrypoint /root/boot/jira+confluence+bitbucket.sh -it ubuntu-tis-full /bin/bash

### Jira + Confluence + Bitbucket + Bamboo
    docker run -p 2430:2430 -p 7990:7990 -p 8080:8080 -p 8090:8090 -p 8085:8085 --entrypoint /root/boot/jira+confluence+bitbucket+bamboo.sh -it ubuntu-tis-full /bin/bash

### Jira + Bitbucket + Bamboo
    docker run -p 2430:2430 -p 7990:7990 -p 8080:8080 -p 8085:8085 --entrypoint /root/boot/jira+bitbucket+bamboo.sh -it ubuntu-tis-full /bin/bash


# Build instructions if you are pulling from Docker Hub (beta)

    docker pull boarderdav/ubuntu-tis-full
    docker run -p 2430:2430 -p 7990-8200:7990-8200 -it boarderdav/ubuntu-tis-full /bin/bash

## Start only the applications you need (beta))
By adding the `--entrypoint` flag, you are able to startup only the applications you need. 

### Jira + Confluence
    docker run -p 2430:2430 -p 8080:8080 -p 8090:8090 --entrypoint /root/boot/jira+confluence.sh -it boarderdav/ubuntu-tis-full /bin/bash

### Jira + Confluence + Bitbucket
    docker run -p 2430:2430 -p 7990:7990 -p 8080:8080 -p 8090:8090 --entrypoint /root/boot/jira+confluence+bitbucket.sh -it boarderdav/ubuntu-tis-full /bin/bash

### Jira + Confluence + Bitbucket + Bamboo
    docker run -p 2430:2430 -p 7990:7990 -p 8080:8080 -p 8090:8090 -p 8085:8085 --entrypoint /root/boot/jira+confluence+bitbucket+bamboo.sh -it boarderdav/ubuntu-tis-full /bin/bash

### Jira + Bitbucket + Bamboo
    docker run -p 2430:2430 -p 7990:7990 -p 8080:8080 -p 8085:8085 --entrypoint /root/boot/jira+bitbucket+bamboo.sh -it boarderdav/ubuntu-tis-full /bin/bash

# Access Teams In Space locally on your computer.

* http://crowd.teamsinspace.com:2430
* http://jira.teamsinspace.com:8080
* http://confluence.teamsinspace.com:8090
* http://bamboo.teamsinspace.com:8085
* http://bitbucket.teamsinspace.com:7990
* http://fecru.teamsinspace.com:8060

___
# Contact
Contact David Jenkins [djenkins@atlassian.com](mailto:djenkins@atlassian.com) if you have any questions, contributions, or concerns with this environment.

# Troubleshooting
If you are having trouble building either docker-java8 or ubuntu-tis-full locally, you should consider removing old docker images. Check to see what images you have on your computer by running 
    docker images

If you see the following output, then you should consider removing docker-java8 and ubuntu-tis-full and then recreate them.
    docker@teamsinspace: ~$ docker images
    REPOSITORY          TAG                 IMAGE ID            CREATED             SIZE
    ubuntu-tis-full     latest              c5cf0a784c5a        10 minutes ago      1.68GB
    docker-java8        latest              fa5fcec56fe9        16 minutes ago      891MB
    ubuntu              14.04               3aa18c7568fc        9 months ago        188MB

Remove images using the following commands
    docker rmi ubuntu-tis-full
    docker rmi docker-java8

Then follow the steps in the Overview section to rebuild those images and start fresh. This will ensure you pull down the latest dependencies that are built in docker-java8 as well as pull down the latest Teams In Space Bitbucket repository when creating the ubuntu-tis-full image. Remember that creating the ubuntu-tis-full image is pulling directly from the repository so you'll want to make sure this is recreated if you want the latest updates of Teams In Space.


