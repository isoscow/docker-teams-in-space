FROM adoptopenjdk/openjdk11:debian
MAINTAINER David Jenkins "djenkins@atlassian.com"

ENV DEBIAN_FRONTEND=noninteractive 
# Install software
RUN apt-get update -y && \
    apt-get install -y \
        libcurl4-openssl-dev libexpat1-dev tcl8.6-dev tcl-dev tk8.6-dev tk-dev tcl8.6-doc tk8.6-doc gettext \
            vim ssh php php-xml zip git git-lfs && \
    apt-get clean all && \
    rm -rf /var/lib/apt/lists/*

#RUN wget "https://mirrors.edge.kernel.org/pub/software/scm/git/git-2.20.0.tar.gz" \
#   && tar xfz git-2.20.0.tar.gz && \
#   cd git* && \
#   ./configure && make && make install


RUN mkdir /root/.ssh/
COPY id_rsa /root/.ssh/id_rsa
RUN chmod 400 /root/.ssh/id_rsa && \
    touch /root/.ssh/known_hosts && \
    ssh-keyscan "bitbucket.org" >> /root/.ssh/known_hosts && \
    echo "starting clonez!" && \
    git clone git@bitbucket.org:atlassian-demo/teams-in-space.git /opt/atlassian && \
    echo "source /opt/atlassian/setup.sh" >> /root/.profile && \
    echo "source /opt/atlassian/setup.sh" >> /root/.bashrc
COPY ./boot/ /root/boot
RUN chmod +x /root/boot/entrypoint.sh && \
    chmod +x /root/boot/startup.sh    && \
    chmod +x /root/boot/*


ENTRYPOINT ["/root/boot/entrypoint.sh"]
